---?image=Puente_Alto/presentaciones/modulo_1/template/img/Code-Background.png

@title[Portada]

@snap[west headline text-white]
TEORÍA DE CONJUNTOS
@snapend

@snap[south-west byline  text-white]

@size[12px](ANALISTA DESARROLLADOR DE APLICACION DE SOFTWARE)
@snapend

---
@title[Slide Markdown]

@snap[north-west]
@css[message-box](Objetivos)
@snapend

@snap[north-west]
@ol
- Aplicar la teoría de conjuntos como una herramienta básica en la formulación y resolución de problemas tanto en el ámbito matemático como en la vida cotidiana. 
- Ilustrar las ventajas, propiedades y utilización de los conceptos de la teoría de conjuntos. 
- Resuelve operaciones de teoría de conjuntos con enunciados relacionados a la vida cotidiana.
- Resuelve operaciones matemáticas simples de teoría de conjuntos. 
@olend
@snapend


---?image=template/img/pencils.jpg
@title[Header + Footer Templates]

## @color[black](Header & Footer<br>Slide Templates)

@fa[arrow-down text-black]

@snap[south docslink span-50]
[The Template Docs](https://gitpitch.com/docs/the-template)
@snapend


+++?image=template/img/bg/orange.jpg&position=top&size=100% 20%
@title[Header Bar + Image Body]

@snap[north text-white span-100]
@size[1.5em](Lorem Ipsum Dolor Sit Amet)
@snapend

@snap[south span-100]
![DATAFLOW](template/img/dataflow.png)
<br><br>
@snapend

@snap[south-west template-note text-gray]
Header bar with image body template.
@snapend


+++?image=template/img/bg/blue.jpg&position=bottom&size=100% 20%
@title[Footer Bar + Image Body]

@snap[south text-white span-100]
@size[1.5em](Lorem Ipsum Dolor Sit Amet)
@snapend

@snap[north span-100]
<br>
![DATAFLOW](template/img/dataflow.png)
@snapend

@snap[north-east template-note text-gray span-40]
Footer bar with image body template.
@snapend


+++?image=template/img/bg/black.jpg&position=center&size=100% 65%
@title[Center Bar + Image Body]

@snap[north span-100]
@size[1.5em](Lorem Ipsum Dolor Sit Amet)
@snapend

@snap[midpoint span-80]
![DATAFLOW](template/img/dataflow.png)
@snapend

@snap[south-west template-note text-gray]
Center bar with image body template.
@snapend


+++?image=template/img/bg/purple.jpg&position=top&size=100% 20%
@title[Header Bar + List Body]

@snap[north text-white span-100]
@size[1.5em](Lorem Ipsum Dolor Sit Amet)
@snapend

@snap[south span-100]
@ol[bullet-green](false)
- Consectetur adipiscing elit
- Sed do eiusmod tempor
- Ut enim ad minim veniam
- Duis aute irure dolor in
- Excepteur sint occaecat
- Cupidatat non proident
- Sunt in culpa qui officia
@olend
<br><br>
@snapend

@snap[south-west template-note text-gray]
Header bar with list body template.
@snapend


+++?image=template/img/bg/green.jpg&position=center&size=100% 65%
@title[Center Bar + List Body]

@snap[north span-100]
@size[1.5em](Lorem Ipsum Dolor Sit Amet)
@snapend

@snap[midpoint text-white span-100]
@ul[header-footer-list-shrink](false)
- Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
- Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
- Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
- Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
@snapend

@snap[south-west template-note text-gray]
Center bar with list body template.
@snapend

